<?php
    class Model {
        private $pdo;

        // Constructor to connect to the database
        public function __construct() {
            $this->pdo = new PDO('mysql:host=localhost;dbname=nobels', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        public function get_last(){
            $sql = "SELECT * FROM nobels ORDER BY year DESC LIMIT 25";
            $result = $this->pdo->query($sql);
            $users = $result->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        }

        public function get_nb_nobel_prizes() {
            $sql = "SELECT count(*) FROM nobels";
            $result = $this->pdo->query($sql);
            $count = $result->fetchColumn();
            return $count;
        }

        public function get_nobel_prize_informations($id) {
            $sql = "SELECT * FROM nobels WHERE id = $id";
            $result = $this->pdo->query($sql);
            if ($result->rowCount() > 0) {
                $user = $result->fetch(PDO::FETCH_ASSOC);
                return $user;
            } else {  
                echo "Aucun identifiant";
                return false;
                
                
            }  
        }

        public function get_category() {
            $sql = "SELECT category FROM nobels";
            $result = $this->pdo->query($sql);
            $category = $result->fetchAll(PDO::FETCH_ASSOC);
            $AllCategory = [];
            foreach ($category as $cat) {
                if (!in_array($cat['category'], $AllCategory)) {
                    // $AllCategory[] = $cat['category'];
                    array_push($AllCategory, $cat['category']);
                }
            }
            return $AllCategory;
        }

        public  function add_nobel_prize($infos) {
            $sql = "INSERT INTO nobels ( name, year, birthdate,    birthplace, county,category,  motivation)
            VALUES ($infos[0], $infos[1], $infos[2],  $infos[3], $infos[4], $infos[5],  $infos[6])";
            echo $sql;
            $result = $this->pdo->query($sql);
            //$users = $result->fetchAll(PDO:FETCH_ASSOC);
            return $result;
        }

    }



    
?>